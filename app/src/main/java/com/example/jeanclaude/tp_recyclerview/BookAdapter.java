package com.example.jeanclaude.tp_recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * l'Adapter a pour rôle de prendre en entrée les données à afficher, puis pour chaque
 * donnée, il crée le layout de la ligne qui va l'afficher et il remplit cette ligne avec
 * les valeurs de la donnée associée.
 */

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {

    // les données à afficher dans le RecyclerView
    private List<Book> bookList;

    public BookAdapter(List<Book> bookList) {
        this.bookList = bookList;
    }

    // création du layout d'une ligne
    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.book_list_row, parent, false);

        return new BookViewHolder(itemView);
    }

    // remplissage d'une ligne du Recycler à partir de la donnée à afficher à la position donnée
    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        holder.title.setText(bookList.get(position).getTitle());
        holder.author.setText(bookList.get(position).getAuthor());
    }

    // nombre d'éléments à gérer dans le ReyclerView (obligatoire sinon liste vide à l'écran !)
    @Override
    public int getItemCount() {
        return bookList.size();
    }

    /**
     * Le ViewHolder définit une donnée à afficher
     */
    public class BookViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView author;

        public BookViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            author = (TextView) view.findViewById(R.id.author);
        }
    }
}
