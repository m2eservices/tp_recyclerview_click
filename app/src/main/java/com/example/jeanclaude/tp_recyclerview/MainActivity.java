package com.example.jeanclaude.tp_recyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Exemple de RecyclerView pour afficher une liste de livres
 */

public class MainActivity extends AppCompatActivity {

    private List<Book> bookList = new ArrayList<>();
    private RecyclerView recyclerView;
    private BookAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // on récupère le RecycleView
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        // On définit un adaptateur de livres avec une liste de livres en entrée
        mAdapter = new BookAdapter(bookList);
        // on définit le layout du RecyclerView comme un Linear (vertical)
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        // on associe des animations par défaut pour le RecyclerView
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        // On associe l'adaptateur de livres au RecyclerView
        recyclerView.setAdapter(mAdapter);

        // gestion du clic sur le recyclerView
        recyclerView.addOnItemTouchListener(new RecyclerViewTouchListener(getApplicationContext(), recyclerView, new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                Toast.makeText(getApplicationContext(), bookList.get(position).getTitle() + " is clicked!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(getApplicationContext(), bookList.get(position).getTitle() + " is long pressed!", Toast.LENGTH_SHORT).show();

            }
        }));

        // initialisation des données en entrée
        initBookData();
    }

    private void initBookData() {
        Book book = new Book("Hello Android", "Ed Burnette");
        bookList.add(book);

        book = new Book("Beginning Android 3", "Mark Murphy");
        bookList.add(book);

        book = new Book("Unlocking Android", " W. Frank Ableson");
        bookList.add(book);

        book = new Book("Android Tablet Development", "Wei Meng Lee");
        bookList.add(book);

        book = new Book("Android Apps Security", "Sheran Gunasekera");
        bookList.add(book);

        book = new Book("Hello Android, V2", "Ed Burnette");
        bookList.add(book);

        book = new Book("Beginning Android 3, V2", "Mark Murphy");
        bookList.add(book);

        book = new Book("Unlocking Android, V2", " W. Frank Ableson");
        bookList.add(book);

        book = new Book("Android Tablet Development, V2", "Wei Meng Lee");
        bookList.add(book);

        book = new Book("Android Apps Security, V2", "Sheran Gunasekera");
        bookList.add(book);

        mAdapter.notifyDataSetChanged();
    }
}
