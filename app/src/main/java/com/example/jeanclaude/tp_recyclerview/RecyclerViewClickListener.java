package com.example.jeanclaude.tp_recyclerview;

import android.view.View;

/**
 * Interface pour gérer le clic sur le RecyclerView
 * il n'existe pas de onClick pour le RecyclerView, et il existe beaucoup de solutions pour
 * répondre à ce besoin...
 */

public interface RecyclerViewClickListener {

    void onClick(View view, int position);

    void onLongClick(View view, int position);
}
